# lowaniot-device-access-sdk

#### 介绍
罗万智联平台设备Mqtt接入-SDK

#### 说明
终端设备对接罗万智联平台，通过Mqtt进行通信。


#### 使用步骤

1.  通过gitee下载[lowan-lib-mqtt](https://gitee.com/hangzhou-luowan-information/lowaniot-device-access-sdk/tree/master/device-demo/src/main/resources/lib)工具包或者联系罗万智联平台工作人员获取`lowan-lib-mqtt`工具包。
2.  联系罗万智联平台工作人员注册罗万智联平台账号，获取到MQTT中需要使用到的账号及密码。
3.  参考demo编写代码，通过MQTT订阅罗万智联平台提供相对应的TOPIC，获取到设备上报消息。

#### 特别注意

1.  **package** **com.lowan.device.config.MqttConfig**中username，password通过罗万智联平台获取。
2.  **com.lowan.device.consumer.MqttSubscribe.topic**中topic通过罗万智联平台获取。
3.  **com.lowan.device.model.DeviceEntity**作为订阅罗万智联平台接收消息的标准格式，其中data由不同的设备而产生不同的内容。
4.  更多详情请咨询罗万智联平台工作人员。


package com.lowan.device.model;

import com.alibaba.fastjson.JSONObject;

/**
 * Title: DeviceEntity.java
 * <p>
 * 终端设备实体类
 * @author y_sp
 * @date 2022 二月
 */
public class DeviceEntity {

    /**
     * 终端设备上报数据 标准的JSON格式
     */
    private JSONObject data;

    /**
     * 设备标识
     */
    private String deviceId;

    /**
     * 消息标识
     */
    private String messageId;

    /**
     * 消息类型
     */
    private String messageType;

    /**
     * 时间戳
     */
    private Long timestamp;

    public DeviceEntity() {
    }

    public DeviceEntity(JSONObject data, String deviceId, String messageId, String messageType, Long timestamp) {
        this.data = data;
        this.deviceId = deviceId;
        this.messageId = messageId;
        this.messageType = messageType;
        this.timestamp = timestamp;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}

package com.lowan.device.config;

import com.lowan.lib.mqtt.MqttConnOpts;

/**
 * Title: MqttConfig.java
 * <p>
 * Mqtt 客户端初始化配置
 * @author y_sp
 * @date 2022 二月
 */
public class MqttConfig {

    /** broker地址 */
    private String broker = "tcp://mqtt.lowaniot.com:1883";
    /** 用户名 */
    private String username = "${username}";
    /** 密 码 */
    private String password = "${password}";

    public MqttConfig() {
        /** Mqtt客户端参数初始化 */
        MqttConnOpts.setParams(broker,username,password);
    }

}
